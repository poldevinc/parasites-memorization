package com.example.parasitologymemorization.parasitologymemorization;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.parasitologymemorization.parasitologymemorization.Database.db_category;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class category extends AppCompatActivity implements View.OnClickListener{

 public long countCategory = db_category.count(db_category.class);

    ArrayList<String> hintlist = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        autoComplete();
        ButterKnife.bind(this);

       // Toast.makeText(this, category.getCategory(), Toast.LENGTH_SHORT).show();
        final View.OnClickListener clicks = null;


        for(int i = 0; countCategory > i ;i++){

            LinearLayout l_layout = (LinearLayout) findViewById(R.id.linear_layout);
            l_layout.setOrientation(LinearLayout.VERTICAL);

            Button btntag = new Button(this);
            hintlist.add(GetCategory(i));
            btntag.setId(i);
            btntag.setText(GetCategory(i));
            btntag.setOnClickListener(this);
            l_layout.addView(btntag);

        }
    }

    public void onClick(View view) {

          view.getId();
          Button button=(Button)findViewById(view.getId());


        List<db_category> subCategoryID = db_category.findWithQuery(db_category.class,
                "Select * from DBCATEGORY where title = ?",button.getText().toString());
        String subCategoryIDintent = Integer.valueOf(subCategoryID.get(0).subCategoryID).toString();
         // Toast.makeText(this, subCategoryIDPass, Toast.LENstartActivityGTH_SHORT).show();

        Intent subcategoryIntent = new Intent(this, subCategory.class);
        subcategoryIntent.putExtra("subCategoryID",subCategoryIDintent);
        startActivity(subcategoryIntent);

    }

    public  String GetCategory(int i){

        db_category.listAll(db_category.class);
        List<db_category> CategoryList = db_category.listAll(db_category.class);
        String tittle = CategoryList.get(i).title;
        return tittle;
    }

    public void autoComplete(){





        MultiAutoCompleteTextView mt=(MultiAutoCompleteTextView)
                findViewById(R.id.multiAutoCompleteTextView);

        mt.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        ArrayAdapter<String> adp=new ArrayAdapter<String>(this,
                R.layout.autocomplete,R.id.tvHintCompletion,hintlist);

        mt.setThreshold(1);
        mt.setAdapter(adp);



    }
    @OnClick(R.id.multiAutoCompleteTextView)
    public void ButtonClick() {

        Toast.makeText(this, "cickcesadwq", Toast.LENGTH_SHORT).show();
    }

}
