package com.example.parasitologymemorization.parasitologymemorization.Database;


import com.orm.SugarRecord;
import com.orm.dsl.Table;

@Table
public class db_subcategory extends SugarRecord {

    public int subCategoryID;
    public String title;
    public String pronunciation;
    public String decription1;
    public String decription2;

    public db_subcategory(){

    }


    public db_subcategory(int subCategoryID,String title,String pronunciation
            ,String decription1 ,String decription2){
        this.subCategoryID = subCategoryID;
        this.title = title;
        this.pronunciation = pronunciation;
        this.decription1 = decription1;
        this.decription2 = decription2;



    }


}
