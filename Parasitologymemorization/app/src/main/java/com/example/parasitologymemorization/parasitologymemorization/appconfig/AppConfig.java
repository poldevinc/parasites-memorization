package com.example.parasitologymemorization.parasitologymemorization.appconfig;


import android.app.Application;

import com.orm.SugarContext;

public class AppConfig extends Application {

    @Override
    public void onCreate(){
        super.onCreate();
        SugarContext.init(this);

    }

    public void OnTerminate(){
        super.onTerminate();
    }
}
