package com.example.parasitologymemorization.parasitologymemorization;

import android.content.Intent;
import android.media.Image;
import android.media.MediaPlayer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.parasitologymemorization.parasitologymemorization.Database.db_category;
import com.example.parasitologymemorization.parasitologymemorization.Database.db_subcategory;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class items extends AppCompatActivity {

    @BindView(R.id.title)
    TextView TVtitle;
    @BindView(R.id.pronoun)
    TextView TVpronouncation;
    @BindView(R.id.desc1)
    TextView TVdesc1;
    @BindView(R.id.desc2)
    TextView TVdesc2;

    @BindView(R.id.itemIV)
    ImageView itemIV;


    String image , title, pronunciation , desc1 ,desc2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items);
        ButterKnife.bind(this);
        GetItem();


         String imageformat = image.replace(" ","_").toLowerCase();
         int resID = getResources().getIdentifier("@drawable/"+ imageformat , null, getPackageName());


         Picasso.with(this).load(resID).into(itemIV);


        String [] titlecap = title.split(" ");
        String titlecapholder = "";
        StringBuilder builder = new StringBuilder();
        for (int i = 0; titlecap.length>i; i++ ) {
            titlecapholder = titlecap[i].substring(0,1).toUpperCase()+titlecap[i].substring(1);
            builder.append(titlecapholder+" ");
        }

        TVtitle.setText(builder);
        TVpronouncation.setText(pronunciation);
        TVdesc1.setText(desc1);
        TVdesc2.setText(desc2);




    }


    @OnClick(R.id.playbtn)
    public void ButtonClick() {

        int resID=getResources().getIdentifier(title.replace(" ","_"), "raw", getPackageName());

        MediaPlayer mediaPlayer=MediaPlayer.create(this,resID);
        mediaPlayer.start();

    }



    public  void GetItem(){

        Intent subcategoryIntent = getIntent();
        String itemname = subcategoryIntent.getStringExtra("item_title").toLowerCase();




        List<db_subcategory> itemlist = db_subcategory.find(db_subcategory.class,"title=?",itemname);

            image = itemlist.get(0).title;
            title = itemlist.get(0).title;
            pronunciation = itemlist.get(0).pronunciation;
            desc1 = itemlist.get(0).decription1;
            desc2 = itemlist.get(0).decription2;

    }

}
