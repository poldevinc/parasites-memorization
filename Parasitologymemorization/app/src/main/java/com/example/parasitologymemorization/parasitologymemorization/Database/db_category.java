package com.example.parasitologymemorization.parasitologymemorization.Database;

import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.Table;

@Table
public class db_category extends SugarRecord  {

    public String title;
    public int subCategoryID;

    public db_category(){

    }


    public db_category(String title,int subCategoryID){
        this.title = title;
        this.subCategoryID = subCategoryID;

    }


}
