package com.example.parasitologymemorization.parasitologymemorization;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.parasitologymemorization.parasitologymemorization.Database.db_subcategory;

import java.util.ArrayList;
import java.util.List;
import butterknife.ButterKnife;

public class subCategory extends AppCompatActivity implements View.OnClickListener {
    GridView gridview;

    private final ArrayList<String> Text = new ArrayList<String>();

    private final ArrayList<Integer> Images = new ArrayList<Integer>();
    Drawable drawable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);
        ButterKnife.bind(this);

        // Toast.makeText(this, category.getCategory(), Toast.LENGTH_SHORT).show();


        //
      //  gridview.setAdapter(new CustomAdapter(this, osNameList, osImages));




        Intent subcategoryIntent = getIntent();
        String subCategoryID = subcategoryIntent.getStringExtra("subCategoryID");
        long countCategory = db_subcategory.count(db_subcategory.class,"sub_Category_ID = "+subCategoryID,null);

        //Toast.makeText(this, subCategoryID, Toast.LENGTH_SHORT).show();
        for(int i = 0; countCategory > i ;i++){
            String txtFormat = GetCategory(i,subCategoryID).toUpperCase();

            String imageformat = GetCategory(i,subCategoryID).replace(" ","_").toLowerCase();
            int resID = getResources().getIdentifier("@drawable/"+ imageformat , null, getPackageName());


            Text.add(txtFormat);
            Images.add(resID);
        }

        initViews();
    }



    private void initViews(){
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(),2);
        recyclerView.setLayoutManager(layoutManager);

        ArrayList<AndroidVersion> androidVersions = prepareData();
        CustomAdapter adapter = new CustomAdapter(getApplicationContext(),androidVersions);
        recyclerView.setAdapter(adapter);

    }
    private ArrayList<AndroidVersion> prepareData(){

        ArrayList<AndroidVersion> android_version = new ArrayList<>();
        for(int i=0;i<Text.size();i++){
            AndroidVersion androidVersion = new AndroidVersion();
            androidVersion.setAndroid_version_name(Text.get(i));
            androidVersion.setAndroid_image_url(Images.get(i));
            android_version.add(androidVersion);
        }
        return android_version;
    }

    public void onClick(View view) {


    }

    public  String GetCategory(int i,String subCategoryID){



        List<db_subcategory> subCategorylist = db_subcategory.findWithQuery(db_subcategory.class,
                "Select * from DBSUBCATEGORY where sub_Category_ID = ?",subCategoryID);
        String subCategorytittle = subCategorylist.get(i).title;


        return subCategorytittle;
    }

}
