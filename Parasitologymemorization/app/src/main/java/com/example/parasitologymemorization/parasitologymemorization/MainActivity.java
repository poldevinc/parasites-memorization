package com.example.parasitologymemorization.parasitologymemorization;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.example.parasitologymemorization.parasitologymemorization.Database.db_category;
import com.orm.SugarRecord;
import com.example.parasitologymemorization.parasitologymemorization.Database.db_subcategory;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;



public class MainActivity extends AppCompatActivity {

   // @OnClick(R.id.Category) Button Category;
   @BindView(R.id.toolbar)
   Toolbar toolbar;

    public long countCategory = db_category.count(db_category.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);






        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);





        if (countCategory==0){
            createCategories();
            createSubCategories();
            
        }else{
           // Del_ORM();
        }

        db_subcategory.listAll(db_subcategory.class);
        List<db_subcategory> CategoryList = db_subcategory.listAll(db_subcategory.class);

            for(int i = 0;  10 >i;i++){
                Log.d("thefuck", CategoryList.get(i).title);

            }

    }


    @OnClick(R.id.toolbar)
    public void navclick() {

        Toast.makeText(this, "lel", Toast.LENGTH_SHORT).show();
    }
    @OnClick(R.id.Category)
    public void ButtonClick() {

        startActivity(new Intent(MainActivity.this, category.class));
    }

    public void Del_ORM(){

        db_category.deleteAll(db_category.class);
        db_subcategory.deleteAll(db_subcategory.class);
    }
    public void createCategories(){
        List<db_category> categories = new ArrayList<>();
        categories.add(new db_category("THE AMEBAS",1));
        categories.add(new db_category("The Flagellates",2));
        categories.add(new db_category("The Hemoflagellates",3));
        categories.add(new db_category("The nematodes",4));
        categories.add(new db_category("The filariae",5));
        categories.add(new db_category("The cestodes",6));
        categories.add(new db_category("The trematodes",7));
        SugarRecord.saveInTx(categories);
    }






    public void createSubCategories(){
        List<db_subcategory> Subcategories = new ArrayList<>();
        Subcategories.add(new db_subcategory(1,"entamoeba histolytica","pronunciation","Lorem ipsum dolor sit amet, consectetur adipiscing elit.","Lorem ipsum dolor sit amet, consectetur adipiscing elit."));
        Subcategories.add(new db_subcategory(1,"naegleria fowleri","pronunciation","Lorem ipsum dolor sit amet, consectetur adipiscing elit.","Lorem ipsum dolor sit amet, consectetur adipiscing elit."));
        Subcategories.add(new db_subcategory(1,"entamoeba coli","pronunciation","Lorem ipsum dolor sit amet, consectetur adipiscing elit.","Lorem ipsum dolor sit amet, consectetur adipiscing elit."));

        Subcategories.add(new db_subcategory(2,"giardia intestinalis","pronunciation","",""));
        Subcategories.add(new db_subcategory(2,"trichomonas tenax","pronunciation","",""));
        Subcategories.add(new db_subcategory(2,"dientamoeba fragilis","pronunciation","",""));

        Subcategories.add(new db_subcategory(3,"trypanosama cruzi","pronunciation","",""));
        Subcategories.add(new db_subcategory(3,"trypanosama rangeli","pronunciation","",""));

        Subcategories.add(new db_subcategory(4,"necator americanus","pronunciation","",""));
        Subcategories.add(new db_subcategory(4,"ascaris lumbricodes","pronunciation","",""));

        Subcategories.add(new db_subcategory(5,"wuchereria bancrofi","pronunciation","",""));
        Subcategories.add(new db_subcategory(5,"brugia malayi","pronunciation","",""));

        Subcategories.add(new db_subcategory(6,"taenia saginata","pronunciation","",""));
        Subcategories.add(new db_subcategory(6,"taenia solium","pronunciation","",""));

        Subcategories.add(new db_subcategory(7,"fasciolopsis buski","pronunciation","",""));
        Subcategories.add(new db_subcategory(7,"fasciola hepatica","pronunciation","",""));




        SugarRecord.saveInTx(Subcategories);
    }
}
