package com.example.parasitologymemorization.parasitologymemorization;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder> {
    private ArrayList<AndroidVersion> android;
    private Context context;

    public CustomAdapter(Context context,ArrayList<AndroidVersion> android) {
        this.android = android;
        this.context = context;
    }

    @Override
    public CustomAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gridlayout, viewGroup, false);
        return new ViewHolder(view);
    }


    public void getView(final int position, View convertView, final ViewGroup parent) {

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                Toast.makeText(context, "clicked", Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public void onBindViewHolder(CustomAdapter.ViewHolder viewHolder, int i) {

        viewHolder.tv_android.setText(android.get(i).getAndroid_version_name());
        Picasso.with(context)
                .load(android.get(i)
                .getAndroid_image_url())
                .resize(240, 120)
                .into(viewHolder.img_android);
    }

    @Override
    public int getItemCount() {
        return android.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_android;
        private ImageView img_android;
        public ViewHolder(View view) {
            super(view);

            tv_android = (TextView)view.findViewById(R.id.tv_android);
            img_android = (ImageView) view.findViewById(R.id.img_android);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent item_title = new Intent(context, items.class);
                    item_title.putExtra("item_title",tv_android.getText().toString());
                    item_title.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(item_title);


                }
            });

        }
    }

}